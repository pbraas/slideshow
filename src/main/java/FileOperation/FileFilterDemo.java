package FileOperation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

public class FileFilterDemo extends JFrame {

    JLabel myLabel;
    JButton myButton;

    JFileChooser chooser;

    FileFilterDemo() {
        super("File Filter Demo");
        myLabel = new JLabel("No file is choosed yet");
        myButton = new JButton("Choose file");

        ActionListener listener
                = (ActionEvent ev) -> {
                    if (FileFilterDemo.this.chooser == null) {
                        chooser = new JFileChooser();
                    }
                    chooser.addChoosableFileFilter(new FilterFile(".java", "Java Source Files(*.java)"));
                    chooser.addChoosableFileFilter(new FilterFile(".txt", "Text Files(*.txt)"));
//filter=new MyFilter();	then filter is equivalent to select all files
                    if (chooser.showDialog(FileFilterDemo.this, "Select this") == JFileChooser.APPROVE_OPTION) {
                        FileFilterDemo.this.myLabel.setText(chooser.getSelectedFile().getPath());
                    }
                };

        myButton.addActionListener(listener);

        add(myLabel, "Center");
        add(myButton, "South");

        setSize(300, 300);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        FileFilterDemo ffd = new FileFilterDemo();
        ffd.setVisible(true);
    }
}
