package FileOperation;

import java.awt.*;
import javax.swing.*;


public class Menu {

    public static void createLookAndFeelMenuItem(JMenu jmenu, Component cmp) {
        final UIManager.LookAndFeelInfo[] infos = UIManager.getInstalledLookAndFeels();

        JRadioButtonMenuItem rbm[] = new JRadioButtonMenuItem[infos.length];
        ButtonGroup bg = new ButtonGroup();
        JMenu tmp = new JMenu("Change Look and Feel");
        tmp.setMnemonic('C');
        for (int i = 0; i < infos.length; i++) {
            rbm[i] = new JRadioButtonMenuItem(infos[i].getName());
            rbm[i].setMnemonic(infos[i].getName().charAt(0));
            tmp.add(rbm[i]);
            bg.add(rbm[i]);
            rbm[i].addActionListener(new ListenerMenu(infos[i].getClassName(), cmp));
        }

        rbm[0].setSelected(true);
        jmenu.add(tmp);

    }

}