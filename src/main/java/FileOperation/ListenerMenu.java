
package FileOperation;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class ListenerMenu implements ActionListener {

    String classname;
    Component component;

    ListenerMenu(String className, Component component) {
        this.component = component;
        this.classname = className;
    }

    public ListenerMenu() {
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        try {
            UIManager.setLookAndFeel(classname);
            SwingUtilities.updateComponentTreeUI(component);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            System.out.println(e);
        }
    }
}
