package FileOperation;

import java.util.Date;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.BadLocationException;

// end defination of class FileOperation
public final class Notepad implements ActionListener, MenuConstants {

    JFrame frame;
    JTextArea textarea;
    JLabel statusBar;

    private final String fileName = "Untitled";
    private final boolean saved = true;
    String applicationName = "Javapad";

    String searchString, replaceString;
    int lastSearchIndex;

    FileOperation fileHandler;
    FontChooser fontDialog = null;
    FindDialog findReplaceDialog = null;
    JColorChooser bcolorChooser = null;
    JColorChooser fcolorChooser = null;
    JDialog backgroundDialog = null;
    JDialog foregroundDialog = null;
    JMenuItem cutItem, copyItem, deleteItem, findItem, findNextItem, replaceItem, gotoItem, selectAllItem;

    Notepad() {
        frame = new JFrame(fileName + " - " + applicationName);
        textarea = new JTextArea(30, 60);
        statusBar = new JLabel("||       Ln 1, Col 1  ", JLabel.RIGHT);
        frame.add(new JScrollPane(textarea), BorderLayout.CENTER);
        frame.add(statusBar, BorderLayout.SOUTH);
        frame.add(new JLabel("  "), BorderLayout.EAST);
        frame.add(new JLabel("  "), BorderLayout.WEST);
        createMenuBar(frame);
        frame.setSize(350, 350);
        frame.pack();
        frame.setLocation(100, 50);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        fileHandler = new FileOperation(this);

        textarea.addCaretListener((CaretEvent e) -> {
            int lineNumber = 0, column = 0, pos = 0;
            try {
                pos = textarea.getCaretPosition();
                lineNumber = textarea.getLineOfOffset(pos);
                column = pos - textarea.getLineStartOffset(lineNumber);
            } catch (BadLocationException excp) {
            }
            if (textarea.getText().length() == 0) {
                lineNumber = 0;
                column = 0;
            }
            statusBar.setText("||       Ln " + (lineNumber + 1) + ", Col " + (column + 1));
        });

        DocumentListener documentListener = new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                fileHandler.saved = false;
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                fileHandler.saved = false;
            }

            public void insertUpdate(DocumentEvent e) {
                fileHandler.saved = false;
            }
        };
        textarea.getDocument().addDocumentListener(documentListener);

        WindowListener frameClose = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                if (fileHandler.confirmSave()) {
                    System.exit(0);
                }
            }
        };
        frame.addWindowListener(frameClose);
    }

    public void goTo() {
        int lineNumber = 0;
        try {
            lineNumber = textarea.getLineOfOffset(textarea.getCaretPosition()) + 1;
            String tempStr = JOptionPane.showInputDialog(frame, "Enter Line Number:", "" + lineNumber);
            if (tempStr == null) {
                return;
            }
            lineNumber = Integer.parseInt(tempStr);
            textarea.setCaretPosition(textarea.getLineStartOffset(lineNumber - 1));
        } catch (NumberFormatException | BadLocationException e) {
        }
    }
//

    @Override
    public void actionPerformed(ActionEvent ev) {
        String cmdText = ev.getActionCommand();

        switch (cmdText) {
            case fileNew:
                fileHandler.newFile();
                break;
            case fileOpen:
                fileHandler.openFile();
                break;
            case fileSave:
                fileHandler.saveThisFile();
                break;
            case fileSaveAs:
                fileHandler.saveAsFile();
                break;
            case fileExit:
                if (fileHandler.confirmSave()) {
                    System.exit(0);
                }
                break;
            case filePrint:
                JOptionPane.showMessageDialog(Notepad.this.frame,
                        "Get ur printer repaired first! It seems u dont have one!",
                        "Bad Printer",
                        JOptionPane.INFORMATION_MESSAGE
                );
                break;
            case editCut:
                textarea.cut();
                break;
            case editCopy:
                textarea.copy();
                break;
            case editPaste:
                textarea.paste();
                break;
            case editDelete:
                textarea.replaceSelection("");
                break;
            case editFind:
                if (Notepad.this.textarea.getText().length() == 0) {
                    return;	// text box have no text
                }
                if (findReplaceDialog == null) {
                    findReplaceDialog = new FindDialog(Notepad.this.textarea);
                }
                findReplaceDialog.showDialog(Notepad.this.frame, true);//find
                break;
            case editFindNext:
                if (Notepad.this.textarea.getText().length() == 0) {
                    return;	// text box have no text
                }
                if (findReplaceDialog == null) {
                    statusBar.setText("Nothing to search for, use Find option of Edit Menu first !!!!");
                } else {
                    findReplaceDialog.findNextWithSelection();
                }
                break;
            case editReplace:
                if (Notepad.this.textarea.getText().length() == 0) {
                    return;	// text box have no text
                }
                if (findReplaceDialog == null) {
                    findReplaceDialog = new FindDialog(Notepad.this.textarea);
                }
                findReplaceDialog.showDialog(Notepad.this.frame, false);//replace
                break;
            case editGoTo:
                if (Notepad.this.textarea.getText().length() == 0) {
                    return;	// text box have no text
                }
                goTo();
                break;
            case editSelectAll:
                textarea.selectAll();
                break;
            case editTimeDate:
                textarea.insert(new Date().toString(), textarea.getSelectionStart());
                break;
            case formatWordWrap: {
                JCheckBoxMenuItem temp = (JCheckBoxMenuItem) ev.getSource();
                textarea.setLineWrap(temp.isSelected());
                break;
            }
            case formatFont:
                if (fontDialog == null) {
                    fontDialog = new FontChooser(textarea.getFont());
                }
                if (fontDialog.showDialog(Notepad.this.frame, "Choose a font")) {
                    Notepad.this.textarea.setFont(fontDialog.createFont());
                }
                break;
            case formatForeground:
                showForegroundColorDialog();
                break;
            case formatBackground:
                showBackgroundColorDialog();
                break;
            case viewStatusBar: {
                JCheckBoxMenuItem temp = (JCheckBoxMenuItem) ev.getSource();
                statusBar.setVisible(temp.isSelected());
                break;
            }
            case helpAboutNotepad:
                JOptionPane.showMessageDialog(Notepad.this.frame, aboutText, "Dedicated 2 u!", JOptionPane.INFORMATION_MESSAGE);
                break;
            default:
                statusBar.setText("This " + cmdText + " command is yet to be implemented");
                break;
        }
    }//action Performed

    void showBackgroundColorDialog() {
        if (bcolorChooser == null) {
            bcolorChooser = new JColorChooser();
        }
        if (backgroundDialog == null) {
            backgroundDialog = JColorChooser.createDialog(Notepad.this.frame,
                    formatBackground,
                    false,
                    bcolorChooser, (ActionEvent evvv) -> {
                        Notepad.this.textarea.setBackground(bcolorChooser.getColor());
                    },
                    null);
        }

        backgroundDialog.setVisible(true);
    }

    void showForegroundColorDialog() {
        if (fcolorChooser == null) {
            fcolorChooser = new JColorChooser();
        }
        if (foregroundDialog == null) {
            foregroundDialog = JColorChooser.createDialog(Notepad.this.frame,
                    formatForeground,
                    false,
                    fcolorChooser, (ActionEvent evvv) -> {
                        Notepad.this.textarea.setForeground(fcolorChooser.getColor());
                    },
                    null);
        }

        foregroundDialog.setVisible(true);
    }

//
    JMenuItem createMenuItem(String s, int key, JMenu toMenu, ActionListener al) {
        JMenuItem temp = new JMenuItem(s, key);
        temp.addActionListener(al);
        toMenu.add(temp);

        return temp;
    }

    JMenuItem createMenuItem(String s, int key, JMenu toMenu, int aclKey, ActionListener al) {
        JMenuItem temp = new JMenuItem(s, key);
        temp.addActionListener(al);
        temp.setAccelerator(KeyStroke.getKeyStroke(aclKey, ActionEvent.CTRL_MASK));
        toMenu.add(temp);

        return temp;
    }

    JCheckBoxMenuItem createCheckBoxMenuItem(String s, int key, JMenu toMenu, ActionListener al) {
        JCheckBoxMenuItem temp = new JCheckBoxMenuItem(s);
        temp.setMnemonic(key);
        temp.addActionListener(al);
        temp.setSelected(false);
        toMenu.add(temp);

        return temp;
    }

    JMenu createMenu(String s, int key, JMenuBar toMenuBar) {
        JMenu temp = new JMenu(s);
        temp.setMnemonic(key);
        toMenuBar.add(temp);
        return temp;
    }

    void createMenuBar(JFrame f) {
        JMenuBar mb = new JMenuBar();
        JMenuItem temp;

        JMenu fileMenu = createMenu(fileText, KeyEvent.VK_F, mb);
        JMenu editMenu = createMenu(editText, KeyEvent.VK_E, mb);
        JMenu formatMenu = createMenu(formatText, KeyEvent.VK_O, mb);
        JMenu viewMenu = createMenu(viewText, KeyEvent.VK_V, mb);
        JMenu helpMenu = createMenu(helpText, KeyEvent.VK_H, mb);

        createMenuItem(fileNew, KeyEvent.VK_N, fileMenu, KeyEvent.VK_N, this);
        createMenuItem(fileOpen, KeyEvent.VK_O, fileMenu, KeyEvent.VK_O, this);
        createMenuItem(fileSave, KeyEvent.VK_S, fileMenu, KeyEvent.VK_S, this);
        createMenuItem(fileSaveAs, KeyEvent.VK_A, fileMenu, this);
        fileMenu.addSeparator();
        temp = createMenuItem(filePageSetup, KeyEvent.VK_U, fileMenu, this);
        temp.setEnabled(false);
        createMenuItem(filePrint, KeyEvent.VK_P, fileMenu, KeyEvent.VK_P, this);
        fileMenu.addSeparator();
        createMenuItem(fileExit, KeyEvent.VK_X, fileMenu, this);

        temp = createMenuItem(editUndo, KeyEvent.VK_U, editMenu, KeyEvent.VK_Z, this);
        temp.setEnabled(false);
        editMenu.addSeparator();
        cutItem = createMenuItem(editCut, KeyEvent.VK_T, editMenu, KeyEvent.VK_X, this);
        copyItem = createMenuItem(editCopy, KeyEvent.VK_C, editMenu, KeyEvent.VK_C, this);
        createMenuItem(editPaste, KeyEvent.VK_P, editMenu, KeyEvent.VK_V, this);
        deleteItem = createMenuItem(editDelete, KeyEvent.VK_L, editMenu, this);
        deleteItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
        editMenu.addSeparator();
        findItem = createMenuItem(editFind, KeyEvent.VK_F, editMenu, KeyEvent.VK_F, this);
        findNextItem = createMenuItem(editFindNext, KeyEvent.VK_N, editMenu, this);
        findNextItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
        replaceItem = createMenuItem(editReplace, KeyEvent.VK_R, editMenu, KeyEvent.VK_H, this);
        gotoItem = createMenuItem(editGoTo, KeyEvent.VK_G, editMenu, KeyEvent.VK_G, this);
        editMenu.addSeparator();
        selectAllItem = createMenuItem(editSelectAll, KeyEvent.VK_A, editMenu, KeyEvent.VK_A, this);
        createMenuItem(editTimeDate, KeyEvent.VK_D, editMenu, this).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));

        createCheckBoxMenuItem(formatWordWrap, KeyEvent.VK_W, formatMenu, this);

        createMenuItem(formatFont, KeyEvent.VK_F, formatMenu, this);
        formatMenu.addSeparator();
        createMenuItem(formatForeground, KeyEvent.VK_T, formatMenu, this);
        createMenuItem(formatBackground, KeyEvent.VK_P, formatMenu, this);

        createCheckBoxMenuItem(viewStatusBar, KeyEvent.VK_S, viewMenu, this).setSelected(true);
        /**
         * **********For Look and Feel, May not work properly on different
         * operating environment**
         */
        Menu.createLookAndFeelMenuItem(viewMenu, this.frame);

        temp = createMenuItem(helpHelpTopic, KeyEvent.VK_H, helpMenu, this);
        temp.setEnabled(false);
        helpMenu.addSeparator();
        createMenuItem(helpAboutNotepad, KeyEvent.VK_A, helpMenu, this);

        MenuListener menuListner = new MenuListener() {
            public void menuSelected(MenuEvent evvvv) {
                if (Notepad.this.textarea.getText().length() == 0) {
                    findItem.setEnabled(false);
                    findNextItem.setEnabled(false);
                    replaceItem.setEnabled(false);
                    selectAllItem.setEnabled(false);
                    gotoItem.setEnabled(false);
                } else {
                    findItem.setEnabled(true);
                    findNextItem.setEnabled(true);
                    replaceItem.setEnabled(true);
                    selectAllItem.setEnabled(true);
                    gotoItem.setEnabled(true);
                }
                if (Notepad.this.textarea.getSelectionStart() == textarea.getSelectionEnd()) {
                    cutItem.setEnabled(false);
                    copyItem.setEnabled(false);
                    deleteItem.setEnabled(false);
                } else {
                    cutItem.setEnabled(true);
                    copyItem.setEnabled(true);
                    deleteItem.setEnabled(true);
                }
            }

            public void menuDeselected(MenuEvent evvvv) {
            }

            public void menuCanceled(MenuEvent evvvv) {
            }
        };
        editMenu.addMenuListener(menuListner);
        f.setJMenuBar(mb);
    }

    /**
     * @param s
     */
    public static void main(String[] s) {
        new Notepad();
    }
}
