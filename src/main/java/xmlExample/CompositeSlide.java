package xmlExample;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

public class CompositeSlide extends SlideComponent {

    protected List<SlideComponentInterface> slideComponents = new ArrayList<>();

    public CompositeSlide(SlideComponentInterface... components) {
        super(0L, 0, 0, Color.BLACK);
        add(components);
    }

    @XmlElement
    public void add(SlideComponentInterface component) {
        slideComponents.add(component);
    }

    @XmlElement
    public void add(SlideComponentInterface... components) {
        slideComponents.addAll(Arrays.asList(components));
    }

    public void clear() {
        slideComponents.clear();
    }

    @Override
    public void paint(Graphics graphics) {
        slideComponents.forEach((slide) -> {
            slide.paint(graphics);
        });
    }

    public List<SlideComponentInterface> getSlideComponents() {
        return slideComponents;
    }

    @XmlElement
    public void setSlideComponents(List<SlideComponentInterface> slideComponents) {
        this.slideComponents = slideComponents;
    }

}
