package xmlExample;

import java.awt.Color;
import javax.xml.bind.annotation.*;

@XmlRootElement
public class Title extends SlideComponent {

    private Long id;
    private String title;
    private int fontSize = 10;

    public Title() {
        super(1l, 40, 60, Color.blue);
    }

    public Title(Long id, String title, int x, int y, Color color) {
        super(id, x, y, color);
        this.title = title;
    }


    public String getTitle() {
        return title;
    }

    @XmlElement
    public void setTitle(String title) {
        this.title = title;
    }

    public int getFontSize() {
        return fontSize;
    }

    @XmlElement(name = "fontsize")
    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

}
