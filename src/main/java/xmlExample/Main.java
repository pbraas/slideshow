package xmlExample;

import java.awt.Color;
import java.io.StringWriter;
import javax.xml.bind.*;

public class Main {
    
    private static final String FILENAME = "slidelist.xml";
    
    public static void main(String[] args) {
        try {
            JAXBContext context = JAXBContext.newInstance(Title.class);
            Marshaller marshaller = context.createMarshaller();
            // enable pretty-print XML output
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            
            StringWriter sw = new StringWriter();
            
            SlideComponentInterface title = new Title(1L, "De titel", 40, 60, Color.blue);
            CompositeSlide slideComponents = new CompositeSlide(title);
            marshaller.marshal(title, sw);
            System.out.println(sw.toString());
            
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
    }
}
