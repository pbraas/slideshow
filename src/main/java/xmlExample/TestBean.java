package xmlExample;

public class TestBean {

    private String name;
    private int age;

    public TestBean() {
        this.name = "";
        this.age = 0;
    }

    public TestBean(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Getter and setter ...
    @Override
    public String toString() {
        return String.format("[TestBean: name='%s', age=%d]", name, age);
    }
}
