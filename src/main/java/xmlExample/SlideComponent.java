package xmlExample;

import java.awt.*;
import javax.xml.bind.annotation.*;

@XmlRootElement
abstract class SlideComponent implements SlideComponentInterface {

    private Long id;
    private int x;
    private int y;
    private Color color;

    public SlideComponent() {
    }

    public Long getId() {
        return id;
    }

    @XmlAttribute
    public void setId(Long id) {
        this.id = id;
    }

    SlideComponent(Long id, int x, int y, Color color) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    @Override
    public void paint(Graphics graphics) {
        graphics.setColor(this.color);
    }

    public int getX() {
        return x;
    }

    @XmlElement
    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    @XmlElement
    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    @XmlElement
    public void setColor(Color color) {
        this.color = color;
    }

}
