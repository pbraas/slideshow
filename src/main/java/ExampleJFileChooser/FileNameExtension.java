/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExampleJFileChooser;

import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Jhaner Fernandez <jhanerfernandez@gmail.com>
 */
public class FileNameExtension implements IFileNameExtension{

    private FileNameExtensionFilter fileNameExtensionFilter;
    
    public FileNameExtension() {        
        fileNameExtensionFilter = new FileNameExtensionFilter("xml", "Html", "txt");
    }
    
    public FileNameExtensionFilter GetFileNameExtensionFilters(){
        return fileNameExtensionFilter;
    }
    
}
