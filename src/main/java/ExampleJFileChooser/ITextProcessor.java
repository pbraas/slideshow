/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExampleJFileChooser;

import java.util.ArrayList;
import java.util.Map;
import slideshow.SlideComponentInterface;
 
public interface ITextProcessor {

    Map<String, SlideDataContract> LoadTxtFile(String fileLocation);
    
    // new method
    ArrayList<SlideComponentInterface> GetSlidesFromTxtFile(String fileLocation);

    boolean CreateTxtFile(Map<String, SlideDataContract> slides, String fileName);
}
