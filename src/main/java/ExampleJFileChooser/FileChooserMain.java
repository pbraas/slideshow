/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExampleJFileChooser;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import slideshow.OrdinarySlideFactory;
import slideshow.SlideAbstractFactoryInterface;

/**
 *
 * @author Jhaner Fernandez <jhanerfernandez@gmail.com>
 */
public class FileChooserMain {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean exits = true;
        try {
            while (exits) {
                // to test the functionality with a console
                System.out.println("please enter: 1 to create txt file");
                System.out.println("please enter: 2 to load txt file");
                System.out.println("please enter: 3 to exit");
                if (scanner.hasNextInt()) {
                    String digit = scanner.nextLine();
                    if ("1".equals(digit)) {
                        CreateTxtFile();
                    }
                    if ("2".equals(digit)) {
                        LoadTxtFile();
                    }
                    if ("3".equals(digit)) {
                        exits = false;
                    }
                } else {
                    System.out.println("Entered an invalid input");
                }
            }
        } catch (IllegalStateException | NoSuchElementException e) {
            // System.in has been closed
            System.out.println("System.in was closed; exiting");
        }

    }

    private static void CreateTxtFile() {

        Map<String, SlideDataContract> slides = new HashMap<String, SlideDataContract>();

        SlideDataContract slide1 = new SlideDataContract();
        slide1.setSlideNumber(1);
        slide1.setTitle("Presentatie");
        slide1.setSubTitle("Slide Show JabberPoint");
        slide1.setLevel1("Welkom! ");
        slide1.setLevel2("Het is een genoegen om u hier te zien!");
        slide1.setLevel3("Datum: 20 OKtober 2020; Presentators: Phillip and Jhaner");
        slide1.setLevel4("Open Universiteit");
        slide1.setFigure("http://www.slide1.nl.image");
        System.out.println("Create a Slide of the type Ordinary");
        SlideAbstractFactoryInterface factory = new OrdinarySlideFactory();
        // fill the slide with the data from the file
        ITextProcessor dataProcessor = new TextProcessor(factory);
        slides.put("Slide", slide1);
        //we expect this to objects Slide info and the slide name
        dataProcessor.CreateTxtFile(slides, "ExampleTxtSlides");
    }

    private static void LoadTxtFile() {
        // abstraction through Interface 
        IFileChooser fileChooser = new FileChooser();
        JFileChooser jfc = fileChooser.GethomeDirectory();
        jfc.setDialogTitle("Select an Text, Html or Xml file");
        jfc.setAcceptAllFileFilterUsed(false);

        // only choose file extensions that we can process
        IFileNameExtension fileNameExtention = new FileNameExtension();
        FileNameExtensionFilter filter = fileNameExtention.GetFileNameExtensionFilters();

        jfc.addChoosableFileFilter(filter);

        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            String fileLocation = jfc.getSelectedFile().getPath();
            System.out.println(fileLocation);
            SlideAbstractFactoryInterface factory = new OrdinarySlideFactory();
            // fill the slide with the data from the file
            ITextProcessor dataProcessor = new TextProcessor(factory);
            dataProcessor.LoadTxtFile(fileLocation);
        }
    }

}
