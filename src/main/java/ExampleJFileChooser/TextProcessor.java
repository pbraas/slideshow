package ExampleJFileChooser;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import slideshow.CompositeSlide;
import slideshow.Figure;
import slideshow.SlideAbstractFactoryInterface;
import slideshow.SlideComponentInterface;
import slideshow.Subtitle;
import slideshow.Text;
import slideshow.Title;

public class TextProcessor implements ITextProcessor {

    private Map<String, SlideDataContract> slides;

    private static final String titleTag = "title";
    private static final String subTitleTag = "subtitle";
    private static final String level1Tag = "level1";
    private static final String level2Tag = "level2";
    private static final String level3Tag = "level3";
    private static final String level4Tag = "level4";
    private static final String figureTag = "figure";
    private static final String emptyString = "";
    // Give a path: manually
    private static final String createFileLocation = "C:\\Users\\jhane\\Desktop\\Example_JFileChooser\\";
    private SlideAbstractFactoryInterface factorySlide;

    public TextProcessor(SlideAbstractFactoryInterface factory) {
        slides = new HashMap<String, SlideDataContract>();
        factorySlide = factory;
    }

    public ArrayList<SlideComponentInterface> GetSlidesFromTxtFile(String fileLocation) {
        try {
            // TODO: Determinate the extenssion of the file
            //System.out.println("Path: " + fileLocation);
            String extensionFile = fileLocation.substring(fileLocation.lastIndexOf("."), fileLocation.length());
            File myObj = new File(fileLocation);

            Scanner myReader = new Scanner(myObj);
            int slideNumber = 1;

            SlideDataContract slideData = null;

            do {
                // add each line to an dictionary to search in this to a tag.
                String data = myReader.nextLine();
                //System.out.println("Value: " + data); // extra data
                if (CheckTextLine(data, "<slide>:") == true) {
                    if (slideData != null) {
                        //printDataObject(slideData);
                        slides.put("slide_" + slideNumber, slideData);
                        slideNumber++;
                    }
                    slideData = new SlideDataContract();
                } else {
                    if (data != null) {
                        CreateSlideMap(data, slideData);
                    }
                }

            } while (myReader.hasNextLine());

            // add the last object to the collection
            if (slideData != null) {
                slides.put("slide_" + slideNumber, slideData);
            }

            myReader.close();
            // helpers to print content
            SlideHelpers.PrintMapContent(slides);
            // Map to array of Slides
            return convertToSlideComponents(slides);
            //
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        return new ArrayList();
    }

    private ArrayList<SlideComponentInterface> convertToSlideComponents(Map<String, SlideDataContract> data) {

        ArrayList<SlideComponentInterface> slidesComponentsList = new ArrayList();
        int t = 80;
        for (Map.Entry<String, SlideDataContract> entry : data.entrySet()) {
            SlideComponentInterface title = new Title(40, t = t + 20, Color.BLUE, 20, entry.getValue().getTitle());
            SlideComponentInterface subtitle = new Subtitle(40, t = t + 20, Color.BLUE, 20, entry.getValue().getSubTitle());
            SlideComponentInterface text1 = new Text(40, t = t + 20, Color.BLUE, 20, entry.getValue().getLevel1());
            SlideComponentInterface text2 = new Text(40, t = t + 20, Color.BLUE, 20, entry.getValue().getLevel2());

            for (String s : SplitTextLine(entry.getValue().getLevel3())) {
                SlideComponentInterface text3 = new Text(40, t = t + 20, Color.BLUE, 20, s);
                slidesComponentsList.add(text3);
            }

            SlideComponentInterface text4 = new Text(40, t = t + 20, Color.BLUE, 20, entry.getValue().getLevel4());
            SlideComponentInterface figure = new Figure(40, t = t + 20, 120, 200, Color.GREEN, false);
            slidesComponentsList.add(text4);
            slidesComponentsList.add(title);
            slidesComponentsList.add(subtitle);
            slidesComponentsList.add(text1);
            slidesComponentsList.add(text2);
            slidesComponentsList.add(figure);
        }
        return slidesComponentsList;
    }

    private String[] SplitTextLine(String text) {
        // It doesnt manner how many spaces are before or after the delimiter,it has to be splitted
        String delimiters = " ;\\s*|\\; \\s*";
        String[] arrOfStr = text.split(delimiters);
        return arrOfStr;
    }

    public Map<String, SlideDataContract> LoadTxtFile(String fileLocation) {
        try {
            // TODO: Determinate the extenssion of the file txt ot html
            String extensionFile = fileLocation.substring(fileLocation.lastIndexOf("."), fileLocation.length());
            File myObj = new File(fileLocation);

            Scanner myReader = new Scanner(myObj);
            int slideNumber = 1;

            SlideDataContract slideData = null;

            do {
                // add each line to an dictionary to search in this to a tag.
                String data = myReader.nextLine();
                System.out.println("Value: " + data); // extra data
                if (CheckTextLine(data, "<slide>:") == true) {
                    if (slideData != null) {
                        //printDataObject(slideData);
                        slides.put("slide_" + slideNumber, slideData);
                        slideNumber++;
                    }
                    slideData = new SlideDataContract();
                } else {
                    if (data != null) {
                        CreateSlideMap(data, slideData);
                    }
                }
            } while (myReader.hasNextLine());

            // add the last object to the collection
            if (slideData != null) {
                slides.put("slide_" + slideNumber, slideData);
            }

            myReader.close();
            // helpers to print content
            SlideHelpers.PrintMapContent(slides);
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return slides;
    } // end;

    public boolean CreateTxtFile(Map<String, SlideDataContract> slides, String fileName) {
        File txtFile = new File(createFileLocation + "" + fileName + ".txt");

        try {
            if (txtFile.createNewFile()) {
                String str = CreateFileString(slides);
                FileOutputStream outputStream = new FileOutputStream(txtFile, false);
                byte[] strToBytes = str.getBytes();
                outputStream.write(strToBytes);
                outputStream.close();
            } else {
                System.out.println("File already exists.");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TextProcessor.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(TextProcessor.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return true;
    }

    private String CreateFileString(Map<String, SlideDataContract> data) {
        String slideString = "";
        String titleString = "";
        String subTitleString = "";
        String level1String = "";
        String level2String = "";
        String level3String = "";
        String level4String = "";
        String figureString = "";

        for (Map.Entry<String, SlideDataContract> entry : data.entrySet()) {
            slideString = CreateTextLine("slide", emptyString);
            titleString = CreateTextLine(titleTag, entry.getValue().getTitle());
            subTitleString = CreateTextLine(subTitleTag, entry.getValue().getSubTitle());
            level1String = CreateTextLine(level1Tag, entry.getValue().getLevel1());
            level2String = CreateTextLine(level2Tag, entry.getValue().getLevel2());
            level3String = CreateTextLine(level3Tag, entry.getValue().getLevel3());
            level4String = CreateTextLine(level4Tag, entry.getValue().getLevel4());
            figureString = CreateTextLine(figureTag, entry.getValue().getFigure());
        }
        return slideString.concat(titleString)
                .concat(subTitleString)
                .concat(level1String)
                .concat(level2String)
                .concat(level3String)
                .concat(level4String)
                .concat(figureString);
    }

    private String CreateTextLine(String tag, String text) {
        // It doesnt manner how many spaces are before or after the delimiter,it has to be splitted
        String delimiters = " ;\\s*|\\; \\s*";
        String[] arrOfStr = text.split(delimiters);
        String newString = "";
        for (String a : arrOfStr) {
            newString += CreateTag(tag).concat(a).concat("\r\n");
        };
        return newString;
    }

    private boolean CheckTextLine(String data, String tagToFind) {
        return data.contains(tagToFind);
    }

    private void CreateSlideMap(String data, SlideDataContract slide) {
        String levelText = "";

        if (CheckTextLine(data, CreateTag(titleTag)) == true) {
            slide.setTitle(data.replace(CreateTag(titleTag), emptyString));
        }
        if (CheckTextLine(data, CreateTag(subTitleTag)) == true) {
            slide.setSubTitle(data.replace(CreateTag(subTitleTag), emptyString));
        }
        if (CheckTextLine(data, CreateTag(level1Tag)) == true) {

            levelText = data.replace(CreateTag(level1Tag), emptyString);

            if (slide.getLevel1() != null) {
                levelText = slide.getLevel1() + ";" + levelText;
            }

            slide.setLevel1(levelText);
        }
        if (CheckTextLine(data, CreateTag(level2Tag)) == true) {

            levelText = data.replace(CreateTag(level2Tag), emptyString);
            if (slide.getLevel2() != null) {
                levelText = slide.getLevel2() + " ; " + levelText;
            }

            slide.setLevel2(levelText);
        }
        if (CheckTextLine(data, CreateTag(level3Tag)) == true) {
            levelText = data.replace(CreateTag(level3Tag), emptyString);
            if (slide.getLevel3() != null) {
                levelText = slide.getLevel3() + " ; " + levelText;
            }

            slide.setLevel3(levelText);
        }
        if (CheckTextLine(data, CreateTag(level4Tag)) == true) {
            levelText = data.replace(CreateTag(level4Tag), emptyString);
            if (slide.getLevel4() != null) {
                levelText = slide.getLevel4() + " ; " + levelText;
            }

            slide.setLevel4(levelText);
        }
        if (CheckTextLine(data, CreateTag(figureTag)) == true) {
            slide.setFigure(data.replace(CreateTag(figureTag), emptyString));
        }
    }

    private String CreateTag(String tag) {
        return "<" + tag + ">:";
    }

}
