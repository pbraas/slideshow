package ExampleJFileChooser;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

public class FileChooser implements IFileChooser {

    private JFileChooser jFileChooser;

    public FileChooser() {
        jFileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
    }

    public JFileChooser GethomeDirectory() {
        return jFileChooser;
    }

}
