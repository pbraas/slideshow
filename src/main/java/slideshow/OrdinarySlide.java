package slideshow;

import java.awt.Color;

public class OrdinarySlide extends SlideComponent {

    public final static int WIDTH = 1200;
    public final static int HEIGHT = 800;

    private String title;
    private String figure;

    // Constructor     
    public OrdinarySlide(int x, int y, Color color) {
        super(x, y, color);
    }

    public void setTitle(String newTitle) {
        this.title = newTitle;
    }

    public void setFigure(String newFigure) {
        this.figure = newFigure;
    }

    public String getTitle() {
        return title;
    }

    public String getFigure() {
        return figure;
    } 
}
