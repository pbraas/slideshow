package slideshow;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import javax.swing.*;

public final class Table extends SlideComponent {

    public int width;
    public int height;
    private JTable table;

    public Table(int x, int y, int width, int height, Color color) {
        super(x, y, color);
        this.width = width;
        this.height = height;
        table = getDefaultTable();
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        int totalWidth = 100;
        int totalHeight = 200;
        BufferedImage tableImage = new BufferedImage(totalWidth, totalHeight,
                BufferedImage.TYPE_INT_RGB);
       graphics.drawImage(tableImage, x, y, color, table);
                
    }

    public JTable getDefaultTable() {
        Vector<String> rowOne = new Vector<>();
        rowOne.addElement("Row1-Column1");
        rowOne.addElement("Row1-Column2");
        rowOne.addElement("Row1-Column3");

        Vector<String> rowTwo = new Vector<>();
        rowTwo.addElement("Row2-Column1");
        rowTwo.addElement("Row2-Column2");
        rowTwo.addElement("Row2-Column3");

        Vector<Vector> rowData = new Vector<>();
        rowData.addElement(rowOne);
        rowData.addElement(rowTwo);

        Vector<String> columnNames = new Vector<>();
        columnNames.addElement("Column One");
        columnNames.addElement("Column Two");
        columnNames.addElement("Column Three");
        return new JTable(rowData, columnNames);
    }

}
