/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slideshow;

import java.awt.Color;

/**
 *
 * @author Jhaner Fernandez <jhanerfernandez@gmail.com>
 */
public class OrdinarySlideFactory implements SlideAbstractFactoryInterface {

    @Override
    public SlideComponentInterface CreateSlide(int x, int y, Color color) {
        return new OrdinarySlide(0, 0, Color.yellow);
    }
    
}
