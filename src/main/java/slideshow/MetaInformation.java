package slideshow;

import java.awt.*;

public class MetaInformation extends SlideComponent {

    public int width;
    public int height;
    private int head;

    public MetaInformation(int x, int y, int width, int height, Color color) {
        super(x, y, color);
        this.width = width;
        this.height = height;
    }

    public int getHead() {
        return head;
    }

    public void setHead(int head) {
        this.head = head;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        graphics.drawRect(x, y, width, height);
    }

}
