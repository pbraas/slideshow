/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slideshow;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 *
 * @author Jhaner Fernandez <jhanerfernandez@gmail.com>
 */
public class Text extends SlideComponent  {
    private int fontSize = 15;
    private String title;

    public Text(int x, int y, Color color, int fontSize, String title) {
        super(x, y, color);
        this.fontSize=fontSize;
        this.title=title;
    }

    public String getText() {
        return title;
    }

    public void setText(String title) {
        this.title = title;
    }

    
    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        graphics.setColor(Color.BLUE);
        graphics.setFont(new Font("Verdana", Font.PLAIN, fontSize));
        graphics.drawString(this.title, x, y);
    }
}
