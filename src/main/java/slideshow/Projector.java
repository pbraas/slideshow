package slideshow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Projector {

    private Screen screen;
    // todo: change this by creating this through a Factory!?
    private final CompositeSlide compositeSlide;
    private final SlideCreator slideCreator;
    JPanel panel;

    public Projector(SlideCreator slideCreator) {
        this.slideCreator = slideCreator;
        compositeSlide = new CompositeSlide();
        screen = new Screen();
        screen.setVisible(true);
    }

    public void loadShapes(SlideComponentInterface... slideComponents) {
        compositeSlide.clear();
        compositeSlide.add(slideComponents);
        screen.repaint();
    }

    // new method
    public void loadShapes(ArrayList<SlideComponentInterface>... slideComponents) {
        compositeSlide.clear();
        for (int i = 0; i < slideComponents.length; i++) {
            System.out.println("********** loadShapes ***************");
            compositeSlide.add(slideComponents[i]);
        }
        screen.repaint();
    }

    private final class Screen extends JFrame {

        private JMenuBar menuBar;
        private JMenuItem menuItemFile;
        private JMenuItem openFile;
        private JMenuItem saveFile;
        private final int WIDTH = 800;
        private final int HEIGHT = 500;

        public Screen() {
            initScreen();
        }

        private void initScreen() {
            setBackground(Color.red);
            menuBar = new JMenuBar();
            menuItemFile = new JMenu();
            openFile = new javax.swing.JMenuItem();
            openFile.setText("Open file");
            openFile.addActionListener((evt) -> this.actionOpenFile(evt));
            menuItemFile.add(openFile);

            saveFile = new javax.swing.JMenuItem();
            saveFile.setText("Save text");
            saveFile.addActionListener((evt) -> this.actionSaveFile(evt));

            menuItemFile.add(saveFile);

            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

            menuItemFile.setText("File");
            menuBar.add(menuItemFile);

            setJMenuBar(menuBar);
            addKeyListener(new KeyController(slideCreator));
            setPreferredSize(new Dimension(WIDTH, HEIGHT));
            pack();
        }

        private void actionOpenFile(ActionEvent evt) {
            System.out.println("Open file");
        }

        private void actionSaveFile(ActionEvent evt) {
            System.out.println("Save file");
        }

        @Override
        public void paint(Graphics graphics) {
            graphics.clearRect(0, 0, WIDTH, HEIGHT);
            super.paint(graphics);
            compositeSlide.paint(graphics);
        }
    }
}
