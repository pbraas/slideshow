package slideshow;

import ExampleJFileChooser.FileChooser;
import ExampleJFileChooser.FileNameExtension;
import ExampleJFileChooser.IFileChooser;
import ExampleJFileChooser.IFileNameExtension;
import ExampleJFileChooser.ITextProcessor;
import ExampleJFileChooser.TextProcessor;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class SlideCreator {

    Projector projector;
    private int slideNumber = 1;
    ArrayList<SlideComponentInterface> slides;

    public SlideCreator() {
        this.projector = new Projector(this);
        this.slides = new ArrayList();
    }

    public void showSlide(int slideNumber) {
        this.slideNumber = slideNumber;
        if (slideNumber == 1) {
            projector.loadShapes(new SpecialSlide().getSlideComponents());
        } else {
            projector.loadShapes(new NormalSlide(slideNumber).getSlideComponents());
        }
    }

    public void nextSlide() {
        slideNumber++;
        showSlide(slideNumber);
    }

    public void previousSlide() {
        slideNumber--;
        showSlide(slideNumber);
    }

    public void startShow() {
        System.out.println("Add slides");
        projector.loadShapes(this.slides);
        // TODO: add slides die opgehaald zijn
    }

    // call this method from the menu to open or search for a file 
    public void openTxtFile() {
        IFileChooser fileChooser = new FileChooser();
        JFileChooser jfc = fileChooser.GethomeDirectory();
        jfc.setDialogTitle("Select an Text, Html or Xml file");
        jfc.setAcceptAllFileFilterUsed(false);

        // only choose file extensions that we can process
        IFileNameExtension fileNameExtention = new FileNameExtension();
        FileNameExtensionFilter filter = fileNameExtention.GetFileNameExtensionFilters();

        jfc.addChoosableFileFilter(filter);

        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            String fileLocation = jfc.getSelectedFile().getPath();

            System.out.println("Create a Slide of the type Ordinary");
            SlideAbstractFactoryInterface factory = new OrdinarySlideFactory();
            // fill the slide with the data from the file
            ITextProcessor dataProcessor = new TextProcessor(factory);
            // get the file from the selected location and generate slide(s)
            this.slides = dataProcessor.GetSlidesFromTxtFile(fileLocation);

        }
    }

}
