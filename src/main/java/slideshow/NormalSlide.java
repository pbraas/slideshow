package slideshow;

import java.awt.Color;

public class NormalSlide extends SlideFactory {

    SlideComponent slideComponents = new CompositeSlide();
    int number;

    public NormalSlide(int number) {
        if (number == 2) {
            this.slideComponents = new CompositeSlide(
                    new Title(40, 100, Color.BLUE, 20, "Slide nummer 2"),
                    new Figure(40, 120, 100, 200, Color.BLUE, true),
                    new Table(160, 300, 100, 200, Color.RED)
            );
        } else if (number == 3) {
            this.slideComponents = new CompositeSlide(
                    new Title(40, 100, Color.BLUE, 20, "Slide nummer 3"),
                    new Figure(40, 120, 100, 200, Color.GREEN, false)
            );
        }
    }

    @Override
    public SlideComponent getSlideComponents() {
        return this.slideComponents;
    }

}
