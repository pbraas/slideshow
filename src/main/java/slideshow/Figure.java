package slideshow;

import java.awt.*;

public class Figure extends SlideComponent {

    private final int width;
    private final int height;
    private boolean rectangle;

    public Figure(int x, int y, int width, int height, Color color, boolean rectangle) {
        super(x, y, color);
        this.width = width;
        this.height = height;
        this.rectangle = rectangle;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        if (this.rectangle) {
            graphics.drawRect(x, y, width, height);
        } else {
            graphics.drawOval(x, y, width, height);
        }
    }

}
