package slideshow;

import java.awt.*;

public class Title extends SlideComponent {

    private int fontSize = 15;
    private String title;

    public Title(int x, int y, Color color, int fontSize, String title) {
        super(x, y, color);
        this.fontSize=fontSize;
        this.title=title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    
    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        graphics.setColor(Color.BLUE);
        graphics.setFont(new Font("Verdana", Font.PLAIN, fontSize));
        graphics.drawString(this.title, x, y);
    }
}
