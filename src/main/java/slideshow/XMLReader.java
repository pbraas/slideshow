package slideshow;

import java.beans.XMLDecoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XMLReader {

    public static void main(String[] args) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File("slides.xml"));
            XMLDecoder decoder = new XMLDecoder(fileInputStream);
            Object object = decoder.readObject();
            System.out.println(object.toString());
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XMLReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
