package slideshow;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class CompositeSlide extends SlideComponent {

    protected List<SlideComponentInterface> slideComponents = new ArrayList<>();

    public CompositeSlide(SlideComponentInterface... components) {
        super(0, 0, Color.BLACK);
        add(components);
    }

    public void add(SlideComponentInterface component) {
        slideComponents.add(component);
    }

    public void add(SlideComponentInterface... components) {
        slideComponents.addAll(Arrays.asList(components));
    }

    public void add(ArrayList<SlideComponentInterface> components) {
        slideComponents.addAll(components);
    }

    public void clear() {
        slideComponents.clear();
    }

    @Override
    public void paint(Graphics graphics) {
        ListIterator<SlideComponentInterface> listIterator = slideComponents.listIterator();
        while (listIterator.hasNext()) {
            listIterator.next().paint(graphics);
        }
    } 
}
