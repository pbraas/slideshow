/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slideshow;

import java.awt.Color;

/**
 *
 * @author Jhaner Fernandez <jhanerfernandez@gmail.com>
 */
public interface SlideAbstractFactoryInterface {
     SlideComponentInterface CreateSlide(int x, int y, Color color);
}
