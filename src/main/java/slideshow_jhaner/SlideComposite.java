package slideshow_jhaner;

import java.awt.*;
import java.util.ArrayList; 
import java.util.List;

public class SlideComposite extends SlideComponent {
	protected List<SlideComponentInterface> children = new ArrayList<>();
	private int currentSlideNumber = 0;

	public SlideComposite(SlideComponentInterface... components) {
		super(Color.BLACK, "CompoundShape", new Level(0, 0));
	}

	public void add(SlideComponentInterface component) {
		children.add(component);
	}

	public int getSize() {
		return children.size();
	}

	public void addList(ArrayList<SlideComponentInterface> components) {
		for (SlideComponentInterface shape : components) {
			children.add(shape);
		} 
	}

	public void nextSlide() {
		if (currentSlideNumber < (children.size() - 1)) {
			setCurrentSlideNumber(currentSlideNumber + 1);
		}
	}

	public void prevSlide() {
		if (currentSlideNumber > 0) {
			setCurrentSlideNumber(currentSlideNumber - 1);
		}
	}

	public void setCurrentSlideNumber(int number) {
		currentSlideNumber = number;
	}

	public SlideComponentInterface getCurrentSlide() {
		return getSlide(currentSlideNumber);
	}

	public void clear() {
		children.clear();
	}

	public SlideComponentInterface getSlide(int number) {
		return children.get(number);
	}

	public int getCurrentSlideNumber() {
		return currentSlideNumber;
	}

	@Override
	public int getX() {
		return 0;
	}

	@Override
	public int getY() {
		return 0;
	}

	@Override
	public void paint(Graphics graphics) {
		if (isSelected()) {
			enableSelectionStyle(graphics);
			graphics.drawString(this.title, getX(), getY());
			//disableSelectionStyle(graphics);
		} 
		// when loading show only the first element
		if (getSize() >= 0) {
			// System.out.println("Call Paint in Compound method");
			// System.out.println("Slide CurrentNumber: " + currentSlideNumber);
			SlideComponentInterface slide = getSlide(getCurrentSlideNumber());
			slide.paint(graphics);
		}
	}

	@Override
	public void appendSlideItem(SlideItem anItem) {
		// TODO Auto-generated method stub
		
	}
}
