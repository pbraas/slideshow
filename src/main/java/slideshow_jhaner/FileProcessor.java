package slideshow_jhaner;

import java.util.ArrayList;

public abstract class FileProcessor {
	
	abstract public ArrayList<SlideComponentInterface> loadFile(String fileLocation);

	abstract public void saveFile(String fileName);
}
