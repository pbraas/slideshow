package slideshow_jhaner;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Vector;

public class Slide extends SlideComponent {

	public String title;
	public Level level;
	protected Vector<SlideItem> items;

	// TODO: do we need the Font Color?
	public Slide(Color color, String title, Level level) {
		super(color, title, level);
		this.level = level;
		this.title = title;
		items = new Vector<SlideItem>();
	}

	public void appendSlideItem(SlideItem anItem) {
		items.addElement(anItem);
	}

	public int getItemsSize() {
		return items.size();
	}

	public Vector<SlideItem> getAllSlideItems() {
		return items;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public int getX() {
		return level.getX();
	}

	@Override
	public int getY() {
		return level.getY();
	}

	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);
		SlideItem slideItem;
		if (getItemsSize() == 0) {
			// show this when there are no slide
			graphics.drawString(this.title, getX(), getY());
		}

		for (int number = 0; number < getItemsSize(); number++) {
			slideItem = (SlideItem) getAllSlideItems().elementAt(number);
			// System.out.println("Slide has Items : " + number);
			graphics.drawString(this.title, getX(), getY());
			graphics.drawString(slideItem.getText(), slideItem.getLevel().getX(), slideItem.getLevel().getY());
		}
	}
}
