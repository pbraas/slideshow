package slideshow_jhaner;


public abstract class SlideItem { 

	public SlideItem() { }  
	 
	public abstract String getText();

	public abstract Level getLevel(); 
}
