package slideshow_jhaner;


import java.awt.*;

public interface SlideComponentInterface {
    int getX();
    int getY(); 
    void select();
    void unSelect();
    boolean isSelected();
    void appendSlideItem(SlideItem anItem);
    String getTitle();
    void paint(Graphics graphics);
}