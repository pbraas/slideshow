package slideshow_jhaner;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class Projector {
	private EditorCanvas canvas;
	private SlideComposite allShapes = new SlideComposite();
	private JFileChooser jFileChooser;
	private FileNameExtensionFilter fileNameExtensionFilter;

	public Projector() {
		canvas = new EditorCanvas();
		allShapes.clear();
		allShapes.add(new Slide(Color.BLACK, "Slide Show Empty", new Level(10, 20)));

	}

	public void loadShapes(ArrayList<SlideComponentInterface> shapes) {
		if (!shapes.isEmpty()) {
			allShapes.clear();
			allShapes.addList(shapes);
			canvas.refresh();
		}
	}

	private class EditorCanvas extends Canvas {
		private static final long serialVersionUID = 1L;
		private JFrame frame;
		private JMenu navigation, file;
		private JMenuItem next, previous, load;

		private static final int PADDING = 1;

		EditorCanvas() {
			createFrame();
			refresh();
		}

		void createFrame() {
			frame = new JFrame();
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.setLocationRelativeTo(null);

			JPanel contentPanel = new JPanel();
			Border padding = BorderFactory.createEmptyBorder(PADDING, PADDING, PADDING, PADDING);
			contentPanel.setBorder(padding);
			frame.setContentPane(contentPanel);
			// Create MenuBar
			JMenuBar mb = new JMenuBar();
			file = new JMenu("File");
			navigation = new JMenu("Navigation");

			next = new JMenuItem("Next");
			previous = new JMenuItem("Previuos");
			load = new JMenuItem("Load");
			// Add Actions
			next.addActionListener((evt) -> this.actionNextSlide(evt));
			previous.addActionListener((evt) -> this.actionPreviousSlide(evt));
			load.addActionListener((evt) -> this.actionLoadSlide(evt));
			navigation.add(next);
			navigation.add(previous);
			file.add(load);

			// the terminates the order
			mb.add(file);
			mb.add(navigation);
			// Add Menu to JFrame
			frame.setJMenuBar(mb);
			frame.add(this);
			frame.setVisible(true);
			frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		}

		void refresh() {
			this.setSize(400, 400);
			frame.pack();
		}

                @Override
		public void paint(Graphics graphics) {
			allShapes.paint(graphics);
		}

		private void actionNextSlide(ActionEvent evt) {
			// System.out.println("Next is Pressed");
			allShapes.nextSlide();
			repaint();
		}

		private void actionPreviousSlide(ActionEvent evt) {
			// System.out.println("Previous is Pressed");
			allShapes.prevSlide();
			repaint();
		}

		private void actionLoadSlide(ActionEvent evt) {
			// System.out.println("Start JFileChooser");
			LoadTxtFile();
			repaint();
		}
	}

	private void LoadTxtFile() { 
		
		jFileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jFileChooser.setDialogTitle("Select an Text, Html or Xml file");
		jFileChooser.setAcceptAllFileFilterUsed(false); 
		fileNameExtensionFilter = new FileNameExtensionFilter("xml", "Html", "txt"); 
		jFileChooser.addChoosableFileFilter(fileNameExtensionFilter);

		int returnValue = jFileChooser.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			String fileLocation = jFileChooser.getSelectedFile().getPath(); 
			// Fill the slide with the data from the file
			FileProcessor dataProcessor = new TxtFileProcessor();
			this.loadShapes(dataProcessor.loadFile(fileLocation));
		}
	}
}
