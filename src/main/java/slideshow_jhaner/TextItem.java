package slideshow_jhaner;


public class TextItem extends SlideItem {
	private String title;
	private final Level level;

	// Constructor
	public TextItem(String title, Level level) {
		this.title = title;
		this.level = level;
	}

        @Override
	public String getText() {
		return title;
	}

	public void setText(String title) {
		this.title = title;
	}

	@Override
	public Level getLevel() { 
		return level;
	}
}