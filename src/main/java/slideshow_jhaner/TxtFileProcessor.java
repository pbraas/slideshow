package slideshow_jhaner;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class TxtFileProcessor extends FileProcessor {

    private Map<String, SlideInternalDataModel> slides;
    private static final String titleTag = "title";
    private static final String subTitleTag = "subtitle";
    private static final String level1Tag = "level1";
    private static final String level2Tag = "level2";
    private static final String level3Tag = "level3";
    private static final String level4Tag = "level4";
    private static final String figureTag = "figure";
    private static final String emptyString = "";

    public TxtFileProcessor() {
        // TODO Auto-generated constructor stub
        slides = new HashMap<String, SlideInternalDataModel>();
    }

    @Override
    public ArrayList<SlideComponentInterface> loadFile(String fileLocation) {
        return loadTxtFile(fileLocation);
    }

    @Override
    public void saveFile(String fileName) {
        // TODO Auto-generated method stub
    }

    private ArrayList<SlideComponentInterface> loadTxtFile(String fileLocation) {
        try {
            String extensionFile = fileLocation.substring(fileLocation.lastIndexOf("."), fileLocation.length());

            if (!extensionFile.toLowerCase().equals(".txt")) {
                return new ArrayList<>();
            }

            File myObj = new File(fileLocation);
            Scanner myReader = new Scanner(myObj);

            int slideNumber = 1;
            SlideInternalDataModel slideData = null;
            do {
                String data = myReader.nextLine();
                if (CheckTagTextLine(data, "<slide>:") == true) {
                    if (slideData != null) {
                        slides.put("slide_" + slideNumber, slideData);
                        slideNumber++;
                    }
                    slideData = new SlideInternalDataModel();
                } else {
                    if (data != null) {
                        CreateSlideMap(data, slideData);
                    }
                }
            } while (myReader.hasNextLine());

            // add the last object to the collection
            if (slideData != null) {
                slides.put("slide_" + slideNumber, slideData);
            }
            myReader.close();
            // helpers to print content
            // SlideHelpers.PrintMapContent(slides);
            // Map to array of Slides
            return convertToSlideComponents(slides);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    private boolean CheckTagTextLine(String data, String tagToFind) {
        return data.contains(tagToFind);
    }

    private void CreateSlideMap(String data, SlideInternalDataModel slide) {
        String levelText = "";

        if (CheckTextLine(data, CreateTag(titleTag)) == true) {
            slide.setTitle(data.replace(CreateTag(titleTag), emptyString));
        }
        if (CheckTextLine(data, CreateTag(subTitleTag)) == true) {
            slide.setSubTitle(data.replace(CreateTag(subTitleTag), emptyString));
        }
        if (CheckTextLine(data, CreateTag(level1Tag)) == true) {

            levelText = data.replace(CreateTag(level1Tag), emptyString);

            if (slide.getLevel1() != null) {
                levelText = slide.getLevel1() + ";" + levelText;
            }

            slide.setLevel1(levelText);
        }
        if (CheckTextLine(data, CreateTag(level2Tag)) == true) {

            levelText = data.replace(CreateTag(level2Tag), emptyString);
            if (slide.getLevel2() != null) {
                levelText = slide.getLevel2() + " ; " + levelText;
            }

            slide.setLevel2(levelText);
        }
        if (CheckTextLine(data, CreateTag(level3Tag)) == true) {
            levelText = data.replace(CreateTag(level3Tag), emptyString);
            if (slide.getLevel3() != null) {
                levelText = slide.getLevel3() + " ; " + levelText;
            }

            slide.setLevel3(levelText);
        }
        if (CheckTextLine(data, CreateTag(level4Tag)) == true) {
            levelText = data.replace(CreateTag(level4Tag), emptyString);
            if (slide.getLevel4() != null) {
                levelText = slide.getLevel4() + " ; " + levelText;
            }

            slide.setLevel4(levelText);
        }
        if (CheckTextLine(data, CreateTag(figureTag)) == true) {
            slide.setFigure(data.replace(CreateTag(figureTag), emptyString));
        }
    }

    private String CreateTag(String tag) {
        return "<" + tag + ">:";
    }

    private boolean CheckTextLine(String data, String tagToFind) {
        return data.contains(tagToFind);
    }

    private ArrayList<SlideComponentInterface> convertToSlideComponents(Map<String, SlideInternalDataModel> data) {
        ArrayList<SlideComponentInterface> slidesComponentsList = new ArrayList<>();
        // Sorted by Key
        Map<String, SlideInternalDataModel> sortedMap = new TreeMap<>(data);
        SlideComponentInterface slide;
        int i = 20;
        for (Map.Entry<String, SlideInternalDataModel> entry : sortedMap.entrySet()) {
            slide = new Slide(Color.BLUE, "Slide", new Level(0, 0));

            if (entry.getValue().getTitle() != null) {
                // System.out.println("title: " + entry.getValue().getTitle());
                slide.appendSlideItem(new TitleItem(entry.getValue().getTitle(), new Level(10, i = i + 20)));
            }

            if (entry.getValue().getSubTitle() != null) {
                // System.out.println("subtitle: " + entry.getValue().getSubTitle());
                slide.appendSlideItem(new SubtitleItem(entry.getValue().getSubTitle(), new Level(10, i = i + 20)));
            }
            if (entry.getValue().getLevel1() != null) {
                // System.out.println("level 1: " + entry.getValue().getLevel1());
                slide.appendSlideItem(new TextItem(entry.getValue().getLevel1(), new Level(10, i = i + 20)));
            }
            if (entry.getValue().getLevel2() != null) {
                // System.out.println("level 2: " + entry.getValue().getLevel2());
                for (String s : SplitTextLine(entry.getValue().getLevel2())) {
                    // System.out.println("level 2: " + s);
                    if (!s.isEmpty()) {
                        slide.appendSlideItem(new TextItem(s, new Level(10, i = i + 20)));
                    }
                }
            }
            if (entry.getValue().getLevel3() != null) {
                // System.out.println("level 3: " + entry.getValue().getLevel3());
                for (String s : SplitTextLine(entry.getValue().getLevel3())) {
                    slide.appendSlideItem(new TextItem(s, new Level(10, i = i + 20)));
                }
            }

            if (entry.getValue().getLevel4() != null) {
                // System.out.println("level 4: " + entry.getValue().getLevel4());
                slide.appendSlideItem(new TextItem(entry.getValue().getLevel4(), new Level(10, i = i + 20)));
            }

            if (entry.getValue().getFigure() != null) {
                // System.out.println("figure : " + entry.getValue().getFigure());
                slide.appendSlideItem(new TextItem(entry.getValue().getFigure(), new Level(10, i = i + 20)));
            }

            slidesComponentsList.add(slide);
            i = 20;
        }
        return slidesComponentsList;
    }

    private String[] SplitTextLine(String text) {
        // It doesnt manner how many spaces are before or after the delimiter,
        // it has to be splitted
        String delimiters = " ;\\s*|\\; \\s*";
        String[] arrOfStr = text.split(delimiters);
        return arrOfStr;
    }

}
