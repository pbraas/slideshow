package slideshow_jhaner;


public class SubtitleItem extends SlideItem {

	private final String subtitle;
	private final Level level;

	// Constructor
	public SubtitleItem(String text, Level level) {
		this.subtitle = text;
		this.level = level;
	}

	@Override
	public String getText() { 
		return subtitle;
	}

	@Override
	public Level getLevel() { 
		return level;
	}

}
